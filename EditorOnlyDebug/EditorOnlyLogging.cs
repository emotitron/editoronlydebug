﻿//Copyright 2018, Davin Carten, All rights reserved

using UnityEngine;
using System.Diagnostics;

/// <summary>
/// These Debug.Log calls are Conditionally purged in all builds other than UNITY_EDITOR. No manual removal needed for production builds.
/// </summary>
public static class DebugEditor
{
	/// <summary>
	/// All calls to this replacement for Debug.LogError is completely purged at build time and only exist in the Editor.
	/// </summary>
	[Conditional("UNITY_EDITOR")]
	public static void LogError(bool show, string errtext)
	{
		if (show)
			UnityEngine.Debug.LogError(errtext);
	}

	/// <summary>
	/// All calls to this replacement for Debug.LogWarning is completely purged at build time and only exist in the Editor.
	/// </summary>	
	[Conditional("UNITY_EDITOR")]
	public static void LogWarning(bool show, string errtext)
	{
		if (show)
			UnityEngine.Debug.LogWarning(errtext);
	}

	/// <summary>
	/// All calls to this replacement for Debug.Log is completely purged at build time and only exist in the Editor.
	/// </summary>	
	[Conditional("UNITY_EDITOR")]
	public static void Log(bool show, string errtext)
	{
		if (show)
			UnityEngine.Debug.Log(errtext);
	}
}
